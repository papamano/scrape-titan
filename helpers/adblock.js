"use strict";

/****** Dependencies ******/

const assert = require("assert");
const AdBlockClient = require("adblock-rs");

const Files = require(__dirname + "/files.js");

/****** Definitions ******/

const EASYLIST_RULES_PATH = __dirname + "/../data/adblock/easylist.txt";

const UBLOCKORIGIN_RULES_PATH = __dirname + "/../data/adblock/unbreak.txt";

/****** Functions ******/

function GetRules(paths) {
    assert(typeof(paths) == "object" && paths.length);

    let rules = [];

    paths.forEach(path => {
        assert(typeof(path) == "string" && path.length);
        const temp = Files.ReadStringSync(path).split("\n");
        rules = rules.concat(temp);
    });

    return rules;
}

function CreateAdBlocker() {
    const rules = GetRules([ EASYLIST_RULES_PATH, UBLOCKORIGIN_RULES_PATH ]);
    assert(typeof(rules) == "object");

    const filterSet = new AdBlockClient.FilterSet(true);
    filterSet.addFilters(rules);

    const client = new AdBlockClient.Engine(filterSet, true);
    assert(typeof(client) == "object" && typeof(client.check) == "function");

    return client;
}

/****** Exports ******/

module.exports = {
    CreateAdBlocker
};