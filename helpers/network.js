"use strict";

/****** Dependencies ******/

const assert = require("assert");
const https  = require("https");

const { ExecuteCmd } = require(__dirname + "/exec.js");

/****** Functions ******/

/* Returns:
 * True: The website is https
 * False: The website is http
 * Null: Could not get website / Timeout
 */
function IsHttps(website, timeout) {
    assert(typeof(website) == "string" && website.length);
    assert(!website.startsWith("https://"));
    assert(!website.startsWith("http://"));
    assert(typeof(timeout) == "number" && timeout > 0);

    return new Promise((resolve, reject) => {

        const target = "https://" + website;

        const req = https.get(target, { timeout }, resp => resolve(true));

        req.on("timeout", _ => {
            req.destroy(Error({ code: "TIMEOUT" }));
        });

        req.on("error", err => {
            if (err.code === "ENOTFOUND" || err.code === "TIMEOUT") resolve(null);
            else resolve(false);
        });

    });
}

function GetProtocol(website, timeout) {
    assert(typeof(website) == "string" && website.length);
    assert(typeof(timeout) == "number" && timeout > 0);

    return new Promise(resolve => {

        IsHttps(website, timeout).then(value => {
            if (value === false) resolve("http://");
            else resolve("https://");
        })
        .catch(err => assert(false, err));
    });
}

function ResolveUrl(url, timeout) {
    assert(typeof(url) == "string" && url.length);
    assert(typeof(timeout) == "number" && timeout > 0);

    const curlCmd = "curl -Ls -o /dev/null --max-time " + timeout + " -w %{url_effective} ";

    return new Promise((resolve, reject) => {
        ExecuteCmd(curlCmd + url)
            .then(resolve)
            .catch(_ => {
                ExecuteCmd(curlCmd + "www." + url).then(resolve).catch(reject);
            });
    });
}

/****** Exports ******/

module.exports = {
    IsHttps,
    GetProtocol,
    ResolveUrl
};