"use strict";

/****** Dependencies ******/

const winston = require("winston");
const assert = require("assert");
const { threadId } = require("worker_threads");

const config = require(__dirname + "/../configs/logs.json");

/****** Definitions ******/

const APPLICATION_NAME = "scrape-titan";

const Colorizer = winston.format.colorize();

const METADATA_MAX_LENGTH = 40;

/****** Functions ******/

/*
 * Levels:
 * error: 0
 * warn: 1
 * info: 2
 * http: 3
 * verbose: 4
 * debug: 5
 * silly: 6
 */

function GetLevel(level, colorize) {
    assert(typeof("level") == "string");
    assert(typeof(colorize) == "boolean");
    const output = level.toUpperCase().padStart(5).substring(0, 5);
    return (colorize ? Colorizer.colorize(level, output) : output);
}

function GetMetadata(meta) {
    if (!meta) return "";
    else return ("[" + String(meta).substring(0, METADATA_MAX_LENGTH) + "] ");
}

function GetThread() {
    return (threadId == 0 ? "  main" : "Thread-" + threadId).padEnd(8).toUpperCase();
}

function GetFilename(moduleObj) {
    assert(typeof(moduleObj) == "object");
    return moduleObj.filename.split("/").slice(-1)[0].padEnd(10).substring(0, 10);
}

function GetFormat(moduleObj, colorize = false) {
    assert(typeof(moduleObj) == "object");

    return winston.format.combine(
        winston.format.label({ label: APPLICATION_NAME }),
        winston.format.timestamp({
            format: "HH:mm:ss MMM-DD-YYYY"
        }),
        winston.format.printf(
            (info) =>
                `[${GetLevel(info.level, colorize)}] ` +
                `[${info.label}] ` +
                `[${process.pid}] ` +
                `[${GetThread()}] ` +
                `[${GetFilename(moduleObj)}] ` +
                `[${info.timestamp}] ` +
                `${GetMetadata(info.metadata)}` +
                `${info.message}`
        ),
    );
}

function CreateTransport(moduleObj, filename, level) {
    assert(typeof(moduleObj) == "object");
    assert(typeof(filename) == "string" && filename.length);
    assert(typeof(level) == "string" && level.length);

    const options = {
        "filename": filename,
        "level": level,
        "format": GetFormat(moduleObj),
        "silent": config.silent
    };

    return new winston.transports.File(options);
}

function CreateConsoleLogger(moduleObj) {
    assert(typeof(moduleObj) == "object");

    return new winston.transports.Console({
        level: "info",
        format: GetFormat(moduleObj, true),
        handleExceptions: true
    });
}

function GetLoggerOptions(moduleObj) {
    assert(typeof(moduleObj) == "object");

    return {
        exitOnError: false,
        transports: [
            CreateTransport(moduleObj, config.infoLogPath, "info"),
            CreateTransport(moduleObj, config.debugLogPath, "debug"),
            CreateTransport(moduleObj, config.errorsLogPath, "warn"),
            CreateConsoleLogger(moduleObj)
        ],
        exceptionHandlers: [
            CreateTransport(moduleObj, config.exceptionsLogPath, "error"),
        ]
    };
}

function CreateLogger(moduleObj, meta) {
    assert(typeof(moduleObj) == "object");

    const options = GetLoggerOptions(moduleObj, meta);
    let logger = winston.createLogger(options);
    return logger;
}

/****** Exports ******/

module.exports = CreateLogger;