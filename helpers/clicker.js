"use strict";

/****** Dependencies ******/

const assert = require("assert");
const Logger = require(__dirname + "/../helpers/logger.js")(module);

/****** Functions ******/

async function ClickOnElement(elem) {
    assert(typeof(elem) == "object");

    try {
        await elem.click();
    } catch(e) {
        await elem.evaluate(x => x.click());
    }
}

async function ClickOnHref(page, url) {
    assert(typeof(page) == "object");
    assert(typeof(url) == "string" && url.length);

    try {
        const pathname = (new URL(url)).pathname;
        const elem = await page.$("a[href*='" + pathname +"']");
        await ClickOnElement(elem);
    } catch (e) {
        Logger.error("Failed to interact with href on '" + page.url() + "': " + e.message);
    }
}

/****** Exports ******/

module.exports = {
    ClickOnHref
};