"use strict";

/****** Dependencies ******/

const assert = require("assert");

const filteredProtocols = require(__dirname + "/../data/filteredUrlTypes.json");

/****** Definitions ******/

const FilteredProtocols = new Set(filteredProtocols);

/****** Functions ******/

/* TODO: Compare with scrapers/urls.s and see which has better performance and
 * which detects more URLs. One of the two should be dropped */

async function GetFilteredURLs(page, filterFunc) {
    assert(typeof(page) == "object");
    assert(typeof(filterFunc) == "function");

    const hrefs = await page.$$eval("a", (links) => links.map((a) => a.href));
    return hrefs.filter(href => {
        try {
            const url = new URL(href);
            const protocol = (new URL(href)).protocol.slice(0, -1);
            return !FilteredProtocols.has(protocol) && filterFunc(url);
        } catch(e) {
            return false;
        }
    });
}

function GetPageHost(page) {
    assert(typeof(page) == "object");
    const pageUrl = new URL(page.url());
    return pageUrl.hostname;
}

async function GetInternalURLs(page) {
    assert(typeof(page) == "object");
    const initialHost = GetPageHost(page);
    const urls = await GetFilteredURLs(page, u => u.hostname === initialHost);
    return urls;
}

async function GetExternalURLs(page) {
    assert(typeof(page) == "object");
    const initialHost = GetPageHost(page);
    const urls = await GetFilteredURLs(page, u => u.hostname !== initialHost);
    return urls;
}

/****** Exports ******/

module.exports = {
    GetInternalURLs,
    GetExternalURLs
};