"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

function SanitizeUrlName(url) {
    assert(typeof(url) == "string");

    let string = url;

    string = string.replace(/\./g, "_").
                    replace(/-/g, "_").
                    replace(/'/g, "_").
                    replace(/\//g, "_").
                    replace(/:/g, "_").
                    replace(/,/g, "_").
                    replace(/\+/g, "p").
                    replace(/\s/g, "_").
                    replace(/\(/g, "_").
                    replace(/\)/g, "_").
                    replace("&", "and");

    /* Remove consecutive underscores */
    string = string.replace(/_+/g, "_");

    /* Remove whitespace */
    string = string.trim();

    /* Make sure that URIs don't end with an underscore */
    if (string.endsWith("_")) string = string.slice(0, -1);

	return string;
}

function RemoveDuplicates(array) {
    assert(typeof(array) == "object");
    assert(typeof(array.filter) == "function");

    return [...(new Set(array))];
}

function IncludesAlpha(text) {
    assert(typeof(text) == "string");
    return /[a-zA-Z]/.test(text);
}

function IncludesDigit(text) {
    assert(typeof(text) == "string");
    return /\d/.test(text);
}

/****** Exports ******/

module.exports = {
    SanitizeUrlName,
    RemoveDuplicates,
    IncludesAlpha,
    IncludesDigit
};