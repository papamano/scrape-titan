"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

function GetMemoryUsage() {
    return process.memoryUsage().heapUsed; // V8 memory usage
}

function GetMemoryUsageMB() {
    return Math.round(GetMemoryUsage() / 1024 /1024);
}

/****** Exports ******/

module.exports = {
    GetMemoryUsage,
    GetMemoryUsageMB
};