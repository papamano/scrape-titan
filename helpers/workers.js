"use strict";

/****** Dependencies ******/

const assert = require("assert");

const Logger = require(__dirname + "/logger.js")(module);

const { Worker } = require("worker_threads");

/****** Functions ******/

function RunService(file, workerData) {
    assert(typeof(file) == "string" && file.length);
    assert(typeof(workerData) == "object");
    assert(typeof(workerData.threadId) == "number" && workerData.threadId >= 0);

    return new Promise((resolve, reject) => {
        const worker = new Worker(file, { workerData });

        worker.on("message", data => {
            Logger.info("Received data from worker " + workerData.threadId);
            resolve(data);
        });

        worker.on("error", error => reject("Error in worker " + workerData.threadId + ": " + String(error)));

        worker.on("exit", code => {
            if (code !== 0)
                reject(new Error("Worker stopped with exit code: " + code));
        });
    });
}

/****** Exports ******/

module.exports = {
    RunService
};