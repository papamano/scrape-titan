"use strict";

/****** Dependencies ******/

const assert = require("assert");
const fs     = require("fs");

/****** Definitions ******/

const JSON_FORMAT_IDENTATION = 2;

/****** Functions ******/

function EnsureDirectoryExists(dir) {
    assert(typeof(dir) == "string" && dir.length);

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
        return true;
    }

    return false;
}

function RemoveDirectory(dir) {
    assert(typeof(dir) == "string" && dir.length);

    /* Setting recursive to true results in behavior similar to the Unix command
     * rm -rf */
    fs.rmdirSync(dir, { recursive: true });
}

function ReadStringSync(path) {
    assert(typeof(path) == "string" && path.length);

    const text = fs.readFileSync(path);
    return String(text);
}

function StoreString(data, filename) {
    assert(typeof(data) == "string");
    assert(typeof(filename) == "string" && filename.length);

    return new Promise((resolve, reject) => {
        fs.writeFile(filename, data, err => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

function ReadJsonSync(path) {
    assert(typeof(path) == "string" && path.length);

    const text = ReadStringSync(path);
    const data = JSON.parse(text);

    assert(typeof(data) == "object");

    return data;
}

function StoreJson(json, filename) {
    assert(typeof(json) == "object");
    assert(typeof(filename) == "string" && filename.length);

    let data = JSON.stringify(json, null, JSON_FORMAT_IDENTATION);
    return StoreString(data, filename);
}

function StoreJsonSync(json, filename) {
    assert(typeof(json) == "object");
    assert(typeof(filename) == "string" && filename.length);

    let data = JSON.stringify(json, null, JSON_FORMAT_IDENTATION);
    fs.writeFileSync(filename, data);
}

function GetDirectoriesInPath(path) {
    assert(typeof(path) == "string" && path.length > 0);

    if (!path.endsWith("/")) path += "/";

    var results = [];

    fs.readdirSync(path).forEach(entry => {

        let file = path + entry;
        let stat = fs.statSync(file);

        if (stat && stat.isDirectory()) results.push(entry);
    });

    return results;
}

function FileExists(path) {
    assert(typeof(path) == "string" && path.length);
    return fs.existsSync(path);
}

function RemoveFile(path, force = false) {
    assert(typeof(path) == "string" && path.length);
    assert(typeof(force) == "boolean");

    /* When "force" is true, exceptions will be ignored if path does not exist */
    fs.rm(path, { "force": force });
}

function RenameFile(oldPath, newPath) {
    assert(typeof(oldPath) == "string" && oldPath.length);
    assert(typeof(newPath) == "string" && newPath.length);

    fs.renameSync(oldPath, newPath);
}

/****** Exports ******/

module.exports = {
    EnsureDirectoryExists,
    RemoveDirectory,
    ReadStringSync,
    StoreString,
    ReadJsonSync,
    StoreJson,
    StoreJsonSync,
    GetDirectoriesInPath,
    FileExists,
    RemoveFile,
    RenameFile
};