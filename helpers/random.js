"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

function RandomPickFromArray(array, count) {
    const shuffled = [...array].sort(() => 0.5 - Math.random());
    return shuffled.slice(0, count);
}

/****** Exports ******/

module.exports = {
    RandomPickFromArray
};