"use strict";

/****** Dependencies ******/

const assert = require("assert");

const { exec } = require("child_process");

/****** Functions ******/

function ExecuteCmd(cmd) {
    assert(typeof(cmd) == "string" && cmd.length);

    return new Promise((resolve, reject) => {

        /* TODO:
         * 1. Fix "dirty" evaluation of execution status
         * 2. Resolve both stdout and sterr through an object
         */
        exec(cmd, (error, stdout, stderr) => {
            if (error){ reject(error);}
            else resolve(stdout ? stdout : stderr);
        });

    });
}

/****** Exports ******/

module.exports = {
    ExecuteCmd
};