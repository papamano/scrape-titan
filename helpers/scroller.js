"use strict";

/****** Dependencies ******/

const assert = require("assert");
const { Wait } = require(__dirname + "/../helpers/time.js");

/****** Functions ******/

function Scroller(timeout) {
    let interval = setInterval(_ => window.scrollBy(0, 50), 100);
    setTimeout(_ => clearInterval(interval), timeout);
}

async function ScrollForMs(page, ms) {
    assert(typeof(page) == "object");
    assert(typeof(page.evaluate) == "function");
    assert(typeof(ms) == "number" && ms > 0);

    await page.evaluate(Scroller, ms);
    await Wait(ms);
}

/****** Exports ******/

module.exports = {
    ScrollForMs
};