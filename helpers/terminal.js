"use strict";

/****** Dependencies ******/

const assert   = require("assert");

/****** Definitions ******/

const FORMAT = {
    COLOR_RED:          "\x1b[31m",
    COLOR_GREEN:        "\x1b[32m",
    COLOR_YELLOW:       "\x1b[33m",
    COLOR_BLUE:         "\x1b[34m",
    COLOR_MAGENTA:      "\x1b[35m",
    COLOR_CYAN:         "\x1b[36m",
    COLOR_BOLD_BLACK:   "\x1b[1m\x1b[30m",
    COLOR_BOLD_RED:     "\x1b[1m\x1b[31m",
    COLOR_BOLD_GREEN:   "\x1b[1m\x1b[32m",
    COLOR_BOLD_YELLOW:  "\x1b[1m\x1b[33m",
    COLOR_BOLD_BLUE:    "\x1b[1m\x1b[34m",
    COLOR_BOLD_MAGENTA: "\x1b[1m\x1b[35m",
    COLOR_BOLD_CYAN:    "\x1b[1m\x1b[36m",
    COLOR_BOLD_WHITE:   "\x1b[1m\x1b[37m",
    FORMAT_RESET:       "\x1b[0m"
}

/****** Functions ******/

function FormatOptions() {
    return FORMAT;
}

function FormatString(text, format) {
    assert(typeof(text) == "string");
    assert(typeof(format) == "string" && format.length);
    assert(Object.values(FORMAT).includes(format));

    return (format + text + FORMAT.FORMAT_RESET);
}

function GetErrorMessagePrefix() {
    return FormatString("> Error", FORMAT.COLOR_BOLD_RED);
}

/****** Exports ******/

module.exports = {
    FormatOptions,
    FormatString,
    GetErrorMessagePrefix
};