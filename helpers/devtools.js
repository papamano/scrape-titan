"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

async function EnableDOM(client) {
    assert(typeof(client) == "object");
    assert(typeof(client.send) == "function");

    await client.send("DOM.enable");
}

async function GetDomRoot(client) {
    assert(typeof(client) == "object");
    assert(typeof(client.send) == "function");

    /*
     * Returns the root DOM node (and optionally the subtree) to the caller.
     *
     * Parameters:
     * depth:  The maximum depth at which children should be retrieved, defaults
     *         to 1. Use -1 for the entire subtree
     * pierce: Whether or not iframes and shadow roots should be traversed when
     *         returning the subtree (default is false).
     */
    let response = await client.send("DOM.getDocument", {
        "pierce": true,
        "depth": -1
    });

    assert(typeof(response) == "object");
    assert(typeof(response.root) == "object");

    return response.root;
}

async function VisitTreeNodes(node, client, visitorFunc, data) {
    assert(typeof(node) == "object");
    assert(typeof(node.nodeId) == "number");
    assert(typeof(client) == "object");
    assert(typeof(client.send) == "function");
    assert(typeof(visitorFunc) == "function")

    let myResult = await visitorFunc(node, client, data);

    let childrenResults = [];

    if (node.contentDocument) {
        let result = await VisitTreeNodes(node.contentDocument, client, visitorFunc, data);
        childrenResults = childrenResults.concat(result);
    }

    if (node.children) {
        for (var i = 0; i < node.children.length; ++i) {
            let child = node.children[i];
            let result = await VisitTreeNodes(child, client, visitorFunc, data);
            childrenResults = childrenResults.concat(result);
        }
    }

    if (node.shadowRoots) {
        for (var i = 0; i < node.shadowRoots.length; ++i) {
            let child = node.shadowRoots[i];
            let result = await VisitTreeNodes(child, client, visitorFunc, data);
            childrenResults = childrenResults.concat(result);
        }
    }

    return (myResult.concat(childrenResults));
}

/****** Exports ******/

module.exports = {
    EnableDOM,
    GetDomRoot,
    VisitTreeNodes
}