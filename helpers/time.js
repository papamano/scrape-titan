"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

async function Wait(ms) {
    assert(typeof(ms) === "number" && ms > 0);

    return new Promise( resolve => setTimeout(resolve, ms) );
}

function StopwatchClick() {
    return new Date();
}

function GetElapsedTime(start, end) {
    assert(typeof(start) == "object");
    assert(typeof(end) == "object");

    let diff = end - start;

    return Math.floor(diff / 1000) + "." + Math.floor((diff % 1000) / 10);
}

function FormatTwoDigits(n) {
    assert(typeof(n) == "number" && n >= 0);
    return (n < 10 ? "0" : "") + n;
}

function GetDateTime(timestamp) {
    let now = (typeof(timestamp) == "number" && timestamp > 0) ? new Date(timestamp) : new Date();

    let date = "";
    date += FormatTwoDigits(now.getHours()) + ":" + FormatTwoDigits(now.getMinutes()) + ":" + FormatTwoDigits(now.getSeconds());
    date += " ";
    date += FormatTwoDigits(now.getDate()) + "/" + FormatTwoDigits(now.getMonth() + 1) + "/" + FormatTwoDigits(now.getFullYear());
    date += " [ " + parseInt((now.getTime() / 1000).toFixed(0)) + " ]";

    return date;
}

function GetTimeStamp() {
    return (new Date()).toLocaleString().replace(", ", "-");
}

/****** Exports ******/

module.exports = {
    Wait,
    GetElapsedTime,
    StopwatchClick,
    GetDateTime,
    GetTimeStamp
};