"use strict";

/****** Dependencies ******/

const assert = require("assert");
const DevtoolsProtocol = require(__dirname + "/../helpers/devtools.js");

/****** Functions ******/

/* This implementation is based on the DOM Domain of the Chrome DevTools
 * Protocol.
 *
 * Backend keeps track of the nodes that were sent to the client and never sends
 * the same node twice. It is client's responsibility to collect information
 * about the nodes that were sent to the client.
 *
 * Note that iframe owner elements will return corresponding document elements
 * as their child nodes.
 *
 * For more information:
 * https://chromedevtools.github.io/devtools-protocol/tot/DOM/
 *
 * This implementation is used to overcome issues with the original
 * page.content() that sometimes hangs unexpectedly.
 * For more information:
 *
 * https://github.com/puppeteer/puppeteer/issues/4011
 * https://github.com/puppeteer/puppeteer/issues/7395
 */

function IsImportantDomNode(name) {
    assert(typeof(name) == "string" && name.length);

    return (
        name.toLowerCase() === "html" ||
        name.toLowerCase() === "script" ||
        name.toLowerCase() === "noscript" ||
        name.toLowerCase() === "style"
    );
}

async function GetNodeContent(node, client) {
    assert(typeof(node) == "object");
    assert(typeof(node.nodeId) == "number");
    assert(typeof(client) == "object");
    assert(typeof(client.send) == "function");

    if (!node.nodeName) return [ ];

    /* Returns node's HTML markup */
    let response = await client.send("DOM.getOuterHTML", {
        "nodeId": node.nodeId
    }).catch(_ => {
        return {
            outerHTML: ""
        };
    });

    assert(typeof(response) == "object");
    assert(typeof(response.outerHTML) == "string");

    if (IsImportantDomNode(node.nodeName)) return [ response.outerHTML + "\n" ];
    else return [ ];
}

async function GetPageContent(page) {
    assert(typeof(page) == "object");
    assert(typeof(page.target) == "function");

    const client = await page.target().createCDPSession();

    await DevtoolsProtocol.EnableDOM(client);

    let root = await DevtoolsProtocol.GetDomRoot(client);
    let content = await DevtoolsProtocol.VisitTreeNodes(root, client, GetNodeContent);

    await client.detach();

    return content.join("\n");
}

/****** Exports ******/

module.exports = {
    GetPageContent
};