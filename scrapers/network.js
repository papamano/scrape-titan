"use strict";

/****** Dependencies ******/

const assert = require("assert");

const { GetDateTime } = require(__dirname + "/../helpers/time.js");

const filteredMimeTypes = require(__dirname + "/../data/filteredMimeTypes.json");

/****** Definitions ******/

const CAPTURE_RESPONSE_BODY = false;

const TRIM_DATA_URLS = true;

const DATA_URLS_MAX_LENGTH = 500;

const FilteredMimeTypes = new Set(filteredMimeTypes);

/****** Functions ******/

/* The default and standard method to capture network traffic is using an event
 * handle for the page object:
 *
 * page.on("request", networkMonitor.RegisterRequest);
 * page.on("response", networkMonitor.RegisterResponse);
 *
 * A different approach is through a Chrome Devtools Protocol session:
 *
 * await client.send('Network.enable');
 * client.on('Network.requestWillBeSent', ProcessRequest);
 */

const NetworkMonitor = function() {

    const state = {
        "requests": [],
        "responses": [],
        "isEnabled": true
    };

    function StripRequest(req, newID) {
        assert(typeof(req) == "object");
        assert(typeof(newID) == "number" && newID >= 0);

        let data = { };

        data.url = req.url();
        data.method = req.method();
        data.resourceType = req.resourceType();
        data.customID = newID;
        data.timestamp = GetDateTime();
        data.headers = req.headers();

        if (req.frame())
            data.frame = { "name": req.frame().name(), "url": req.frame().url() };

        data.isNavigationRequest = req.isNavigationRequest();
        data.redirectChainLength = req.redirectChain().length;
        data.requestID = req._requestId;
        data.postData = req.postData();

        return data;
    }

    function IsBinaryResponse(resp) {
        assert(typeof(resp) == "object");

        const headers = resp.headers();

        // If there is no info about the content type, assume it is not binary
        if (!headers || !headers["content-type"]) return false;

        return (
            FilteredMimeTypes.has(headers["content-type"]) ||
            headers["content-type"].includes("image")      || // Image or graphical data
            headers["content-type"].includes("audio")      || // Audio or music data
            headers["content-type"].includes("video")      || // Video data or files
            headers["content-type"].includes("font")          // Font/typeface data
        );
    }

    async function StripResponse(resp, newID) {
        assert(typeof(resp) == "object");
        assert(typeof(newID) == "number" && newID >= 0);

        let data = {};

        data.url = resp.url();
        data.status = resp.status();
        data.statusText = resp.statusText();
        data.requestID = resp.request()._requestId;
        data.customID = newID;
        data.timestamp = GetDateTime();

        if (resp.frame())
            data.frame = { "name": resp.frame().name(), "url": resp.frame().url() };

        data.fromCache = resp.fromCache();
        data.fromServiceWorker = resp.fromServiceWorker();
        data.headers = resp.headers();
        data.successful = resp.ok();
        data.remoteAddress = resp.remoteAddress();
        data.redirectChain = resp.request().redirectChain().map(req => req.url());

        if (resp.securityDetails())
            data.securityDetails = {
                "issuer": resp.securityDetails().issuer(),
                "protocol": resp.securityDetails().protocol(),
                "subjectName": resp.securityDetails().subjectName(),
                "validFrom": resp.securityDetails().validFrom(),
                "validTo": resp.securityDetails().validTo()
            };

        if (CAPTURE_RESPONSE_BODY && !IsBinaryResponse(resp))
            data.text = await resp.text().catch(e => "");
        else
            data.text = "";

        if (TRIM_DATA_URLS && data.url.startsWith("data:"))
            data.url = data.url.substring(0, DATA_URLS_MAX_LENGTH);

        return data;
    }

    function RegisterRequest(req) {
        assert(typeof(req) == "object");

        if (!state.isEnabled) return;

        let data = StripRequest(req, state.requests.length);
        state.requests.push(data);
    }

    async function RegisterResponse(resp) {
        assert(typeof(resp) == "object");

        if (!state.isEnabled) return;

        let data = await StripResponse(resp, state.responses.length);
        state.responses.push(data);
    }

    function GetRequests() {
        return state.requests;
    }

    function GetResponses() {
        return state.responses;
    }

    function StopRegistering() {
        state.isEnabled = false;
    }

    return {
        RegisterRequest,
        RegisterResponse,
        GetRequests,
        GetResponses,
        StopRegistering
    };

};

/****** Exports ******/

module.exports = {
    NetworkMonitor
};