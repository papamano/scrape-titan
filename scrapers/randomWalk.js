"use strict";

/****** Dependencies ******/

const assert = require("assert");
const Logger = require(__dirname + "/../helpers/logger.js")(module);
const { RandomPickFromArray } = require(__dirname + "/../helpers/random.js");
const { Wait } = require(__dirname + "/../helpers/time.js");
const { ClickOnHref } = require(__dirname + "/../helpers/clicker.js");
const { GetInternalURLs, GetExternalURLs } = require(__dirname + "/../helpers/urls.js");

/****** Definitions ******/

const INTERNAL_PAGES_TO_VISIT = 20;

const EXTERNAL_PAGES_TO_VISIT = 20;

const NAVIGATION_WAIT_TIME = 1500;

const CLICK_NAVIGATION_TIMEOUT = 10000;

const NAVIGATION_TIMEOUT = 30000;

/****** Functions ******/

async function NavigateWithClick(page, url) {
    assert(typeof(page) == "object");
    assert(typeof(url) == "string" && url.length);

    let waitUntilLoad = new Promise((resolve, reject) => {
        page.on("load", resolve);
        setTimeout(resolve, CLICK_NAVIGATION_TIMEOUT);
    });

    Logger.debug("Clicking on URL '" + url + "' in page '" + page.url() + "'");
    await ClickOnHref(page, url);
    await waitUntilLoad;
    await Wait(NAVIGATION_WAIT_TIME);
}

async function GoToUrl(page, url) {
    assert(typeof(page) == "object");
    assert(typeof(url) == "string" && url.length);

    try {
        await page.goto(url, {
            waitUntil: "load",
            timeout: NAVIGATION_TIMEOUT
        });
        await Wait(NAVIGATION_WAIT_TIME);
    } catch(e) {
        Logger.error("Failed to navigate to URL '" + url + "' in page '" + page.url() + "': " + e.message);
    }
}

async function NavigateInternalCollectExternal(page) {
    assert(typeof(page) == "object");

    let eURLs = [];

    for (var i = 0; i < INTERNAL_PAGES_TO_VISIT; ++i) {
        const internalURLs = await GetInternalURLs(page);
        const externalURLs = await GetExternalURLs(page);
        eURLs = eURLs.concat(externalURLs);

        const nextPage = RandomPickFromArray(internalURLs, 1)[0];
        if (!nextPage) break;

        try {
            Logger.debug("Navigating to '" + nextPage + "'")
            await GoToUrl(page, nextPage);
        } catch(e) {
            Logger.error("Failed to navigate to URL '" + nextPage + "' in page '" + page.url() + "': " + e.message);
        }
    }

    return [...(new Set(eURLs))];
}

async function VisitExternal(page, pageQueue) {
    assert(typeof(page) == "object");
    assert(typeof(pageQueue) == "object");

    for(const url of pageQueue) {
        Logger.debug("Visiting '" + url + "'")
        await GoToUrl(page, url);
    }
}

async function DoRandomWalk(page) {
    assert(typeof(page) == "object");

    let externalURLs = await NavigateInternalCollectExternal(page);
    let pageQueue = RandomPickFromArray(externalURLs, EXTERNAL_PAGES_TO_VISIT);
    await VisitExternal(page, pageQueue);
}

/****** Exports ******/

module.exports = {
    DoRandomWalk
}