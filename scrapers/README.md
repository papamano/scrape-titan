# About

The files in this directory contain the source code for the scraping operations. Each module in this direcrory is responsible for collecting data from the visited websites.

# Collected Data

### HTML Code
* HTML code of both the visited website and iFrames.

### HTTP(S) Requests
* Request URL
* Method
* Resource Type
* Custom ID (serial number assigned incrementally)
* Timestamp (generic date-time and UNIX-like timestamp)
* Headers
* Browser frame information (*if available*)
* `Is Navigation Request` Flag
* Redirection Chain Length
* Request ID
* POST Data (*if available*)

### HTTP(S) Responses

* Request URL
* Status
* Status Text
* Request ID
* Custom ID (serial number assigned incrementally)
* Timestamp (generic date-time and UNIX-like timestamp)
* Browser frame information (*if available*)
* `From Cache` Flag
* `From Service Worker` Flag
* Headers
* Success flag
* Remote Address information
* Redirection Chain
* Security Details (*if available*)
* Response body (*if not filtered*)

### Cookies
* Name
* Value
* Domain
* Path
* Expiration
* Size
* HTTP Only flag
* Secure flag
* Session Flag
* Same party flag
* Source Scheme
* Source Port

**Please note that first-party and third-party cookies are stored in different files**.

### JS Functions
* ID
* Function name
* Script ID
* Script URL
* Line number (*if available*)
* Column number (*if available*)
* Hit cout
* Children IDs

### ads.txt Files
* Content of ads.txt file (*if available*)
* Network transaction information

### Screenshots
* Screenshot of landing page