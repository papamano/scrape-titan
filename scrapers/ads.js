"use strict";

/****** Dependencies ******/

const assert = require("assert");

const { CreateAdBlocker } = require(__dirname + "/../helpers/adblock.js");
const Logger = require(__dirname + "/../helpers/logger.js")(module);

/****** Definitions ******/

const AdBlock = CreateAdBlocker();

/****** Functions ******/

function IsSubDomain(url, firstParty) {
    assert(typeof(url) == "string" && url.length);
    assert(typeof(firstParty) == "string" && firstParty.length);

    let domain = (new URL(url)).hostname;
    firstParty = (new URL(firstParty)).hostname;

    if (firstParty.startsWith("www.")) firstParty = firstParty.substr(4);
    if (firstParty.startsWith("www2.")) firstParty = firstParty.substr(5);
    if (domain.startsWith("www.")) domain = domain.substr(4);
    if (domain.startsWith("www2.")) domain = domain.substr(5);

    return domain.endsWith(firstParty);
}

function IsThirdPartyUrl(url, firstParty) {
    assert(typeof(url) == "string" && url.length);
    assert(typeof(firstParty) == "string" && firstParty.length);

    if (IsSubDomain(url, firstParty)) return false;

    let domain = (new URL(url)).hostname;
    firstParty = (new URL(firstParty)).hostname;

    return (
        domain !== firstParty &&
        domain !== "www." + firstParty &&
        firstParty !== "www." + domain &&
        domain !== "www2." + firstParty &&
        firstParty !== "www2." + domain
    );
}

function IsActualAd(url) {
    assert(typeof(url) == "string" && url.length);

    url = url.trim();

    return !(
        url.endsWith(".js") ||
        url.endsWith(".css") ||
        url.endsWith(".woff2") ||
        url.includes("adssettings.google.com") ||
        url.includes("help.revcontent.com") ||
        url.includes("revcontent.com/popup?utm_source=ads&utm_medium=organic&utm_term=signup") ||
        url.includes("mgid.com/services/privacy-policy") ||
        url.includes("adform.com/privacy/adchoices/") ||
        url.includes("mediba.jp/privacy/sp/advertising.html") ||
        url.includes("widgets.adskeeper.com/") ||
        url.includes("youtube.com") ||
        url.includes("www.linkedin.com/") ||
        url.includes("www.instagram.com/") ||
        url.includes("popup.taboola.com") ||
        url.includes("widgets.mgid.com") ||
        url.includes("adyoulike.com/privacy_policy") ||
        url.includes(".safeframe.googlesyndication.com") ||
        url.startsWith("mailto:") ||
        url.startsWith("javascript:")
    );
}

function GetAdUrls(adblock, urls) {
    assert(typeof(adblock) == "object");
    assert(typeof(adblock.check) == "function");
    assert(typeof(urls) == "object");
    assert(typeof(urls.filter) == "function");

    const ads = urls.filter(url => adblock.check(url, "", "", false));
    return ads;
}

function GetAdResponses(adblock, responses, urls) {
    assert(typeof(adblock) == "object");
    assert(typeof(adblock.check) == "function");
    assert(typeof(responses) == "object");
    assert(typeof(responses.forEach) == "function");
    assert(typeof(urls) == "object");
    assert(typeof(urls.forEach) == "function");

    const result = [];

    responses.forEach(response => {

        const requestURLs = [ response.url ].concat(response.redirectChain);
        let badRequest = false;

        requestURLs.forEach(url => {
            if (adblock.check(url, "", "", false)) badRequest = true;
        });

        if (!badRequest) return;

        urls.forEach(url => {
            const targetUrl = (url.length > 100) ? url.substr(0, Math.floor(0.75 * url.length)) : url;

            if (response.text.includes(targetUrl)) result.push(url);
        });
    });

    return result;
}

function FilterAds(urls) {
    assert(typeof(urls) == "object");
    assert(typeof(urls.filter) == "function");

    const result = urls.filter(url => IsActualAd(url));
    return [...(new Set(result))];
}

async function GetPageAds(links, firstParty, responses) {
    assert(typeof(links) == "object");
    assert(typeof(firstParty) == "string" && firstParty.length);
    assert(typeof(responses) == "object");

    const urls = links.filter(url => IsThirdPartyUrl(url, firstParty));

    const adUrls = GetAdUrls(AdBlock, urls);
    const adResponses = GetAdResponses(AdBlock, responses, urls);

    const ads = FilterAds(adUrls.concat(adResponses));

    return ads;
}

async function VisitAds(page, ads, timeout) {
    assert(typeof(page) == "object");
    assert(typeof(page.goto) == "function");
    assert(typeof(timeout) == "number" && timeout > 0);

    const result = [];

    for (const link of ads) {
        try {
            await page.goto(link, { waitUntil: "load" });

            if (link.includes("trends.revcontent"))
                await page.waitForNavigation({ "timeout": 10000, waitUntil: "domcontentloaded" });

            await page.waitForTimeout(timeout);

            result.push({
                "adUrl": link,
                "adLandingPage": page.url()
            });

        } catch(e) {
            Logger.error("Failed to visit ad URL '" + link + "': " + String(e));
        }
    }

    return result;
}

/****** Exports ******/

module.exports = {
    GetPageAds,
    VisitAds
};