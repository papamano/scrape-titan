"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Functions ******/

async function TakeScreenshot(page, path, timeout, filename = "/screenshot.jpg", quality = 90) {
    assert(typeof(page) == "object");
    assert(typeof(page.screenshot) == "function");
    assert(typeof(path) == "string")
    assert(typeof(timeout) == "number" && timeout > 0);
    assert(typeof(filename) == "string" && filename.length);
    assert(typeof(quality) == "number" && quality > 0 & quality <= 100);

    return new Promise(async (resolve, reject) => {

        await page.screenshot({
            "path": path + filename,
            "type": "jpeg",
            "quality": quality,
            "fullPage": true
        }).catch(reject);
        resolve(true);

        setTimeout(_ => reject("Screenshot timeout exceeded"), timeout);
    });
}

/****** Exports ******/

module.exports = {
    TakeScreenshot
};