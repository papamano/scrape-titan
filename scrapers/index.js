"use strict";

/****** Dependencies ******/

const html = require(__dirname + "/html.js");
const cookies = require(__dirname + "/cookies.js");
const adsTxt = require(__dirname + "/adsTxt.js");
const network = require(__dirname + "/network.js");
const functions = require(__dirname + "/functions.js");
const screenshot = require(__dirname + "/screenshot.js");
const urls = require(__dirname + "/urls.js");
const ads = require(__dirname + "/ads.js");
const walk = require(__dirname + "/randomWalk.js")

/****** Exports ******/

module.exports = {
    "GetPageContent": html.GetPageContent,
    "GetAllCookies": cookies.GetAllCookies,
    "GetFirstPartyCookies": cookies.GetFirstPartyCookies,
    "GetThirdPartyCookies": cookies.GetThirdPartyCookies,
    "GetAdsTxt": adsTxt.GetAdsTxt,
    "IsInvalidAdsTxt": adsTxt.IsInvalidAdsTxt,
    "NetworkMonitor": network.NetworkMonitor,
    "GetCertificate": network.GetCertificate,
    "StartProfiler": functions.StartProfiler,
    "GetFunctions": functions.GetFunctions,
    "NativeFunctionsDetector": functions.NativeFunctionsDetector,
    "TakeScreenshot": screenshot.TakeScreenshot,
    "GetPageLinks": urls.GetPageLinks,
    "GetPageAds": ads.GetPageAds,
    "VisitAds": ads.VisitAds,
    "DoRandomWalk": walk.DoRandomWalk
};