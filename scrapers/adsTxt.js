"use strict";

/****** Dependencies ******/

const assert = require("assert");

const { ExecuteCmd }  = require(__dirname + "/../helpers/exec.js");

/****** Functions ******/

function FormRequestCommand(targetURL, directory) {
    assert(typeof(targetURL) == "string" && targetURL.length);
    assert(typeof(directory) == "string" && directory.length);

    return "curl --max-time 30 --retry 1 -vL --fail " + targetURL +
           ' --user-agent "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0"' +
           " -o " + directory + "/ads.txt" +
           " 2> " + directory + "/adsTxtInfo.log" +
           " -w %{content_type}";
}

async function GetAdsTxt(targetURL, directory) {
    assert(typeof(targetURL) == "string" && targetURL.length);
    assert(typeof(directory) == "string" && directory.length);

    try {
        const adsUrl = (targetURL.endsWith("/")) ? (targetURL + "ads.txt") : (targetURL + "/ads.txt");
        assert(typeof(adsUrl) == "string" && adsUrl.length);

        const cmd = FormRequestCommand(adsUrl, directory);
        assert(typeof(cmd) == "string" && cmd.length);

        let responseType = await ExecuteCmd(cmd);

        return {
            "success": true,
            "contentType": responseType
        };

    } catch (err) {
        return {
            "success": false,
            "error": err
        };
    }
}

function IsInvalidAdsTxt(retrievedAds, acceptOnlyTextAdsTxt) {
    assert(typeof(retrievedAds) == "object");
    assert(typeof(retrievedAds.success) == "boolean");
    assert(!(typeof(retrievedAds.success) == true) || typeof(retrievedAds.contentType) == "string");
    assert(typeof(acceptOnlyTextAdsTxt) == "boolean");

    return (
        retrievedAds.success &&
        acceptOnlyTextAdsTxt &&
        !retrievedAds.contentType.includes("text/plain")
    );
}

/****** Exports ******/

module.exports = {
    GetAdsTxt,
    IsInvalidAdsTxt
};