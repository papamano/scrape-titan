"use strict";

/****** Dependencies ******/

const assert = require("assert");
const DevtoolsProtocol = require(__dirname + "/../helpers/devtools.js");

/****** Definitions ******/

function IsInvalidUrl(url) {
    assert(typeof(url) == "string");

    url = url.trim();

    return (
        url.length === 0 ||
        url.startsWith("javascript:") ||
        url === "#" ||
        url === "https:" ||
        url === "http:"
    );
}

async function GetClientSidePageLinks(page) {
    assert(typeof(page) == "object");
    assert(typeof(page.evaluate) == "function");

    const pageLinks = await page.evaluate(_ => {
        let result = [];

        /* Get links in main page */
        let links = document.links;
        for (const l of links) result.push(l.href);

        /* Get links in iFrames */
        let frames = document.getElementsByTagName("iframe");
        for (const frame of frames) {
            let document = frame.contentDocument;
            if (document) {
                let iframeLinks = document.getElementsByTagName("a");
                for (const l of iframeLinks) result.push(l.href);
            }
        }

        return result;
    });

    const filtered = pageLinks.filter(link => link.length > 0);
    return [...new Set(filtered)];
}

async function GetFrameUrls(frame) {
    assert(typeof(frame) == "object");
    assert(typeof(frame.$$eval) == "function");

    return new Promise(async (resolve, reject) => {
        /* A timeout is necessary because sometimes there are issues with
         * puppeteer frames hanging.
         *
         * For more information:
         * https://github.com/puppeteer/puppeteer/issues/3261
         * https://github.com/puppeteer/puppeteer/issues/4367
         */
        setTimeout(_ => reject("Frame Evaluation Timeout"), 1000);

        const hrefs = await frame.$$eval('a', as => as.map(a => a.href)).catch(reject);
        resolve(hrefs);
    });
}

async function GetBrowserSidePageLinks(page) {
    assert(typeof(page) == "object");
    assert(typeof(page.frames) == "function");

    let result = [];

    const frames = await page.frames();
    assert(typeof(frames) == "object");

    for(const frame of frames) {
        if (frame.isDetached()) continue;

        const hrefs = await GetFrameUrls(frame).catch(e => []);
        result = result.concat(hrefs);
    }

    return [...new Set(result)];
}

function FilterPageLinks(pageLinks) {
    assert(typeof(pageLinks) == "object");
    assert(typeof(pageLinks.filter) == "function");

    const filtered = pageLinks.map(link => link.trim())
                              .filter(link => link.length > 0)
                              .filter(link => !IsInvalidUrl(link))
                              .filter(link => !link.startsWith("javascript:"))
                              .filter(link => link !== "https:" && link !== "#");

    return [...new Set(filtered)];
}

async function GetPageLinks(page) {
    assert(typeof(page) == "object");
    assert(typeof(page.target) == "function");

    let pageLinks = await GetBrowserSidePageLinks(page);

    let pageSideLinks = await GetClientSidePageLinks(page);
    pageLinks = pageLinks.concat(pageSideLinks);

    return FilterPageLinks(pageLinks).sort();
}

module.exports = {
    GetPageLinks
};