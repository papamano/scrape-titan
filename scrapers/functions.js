"use strict";

/****** Dependencies ******/

const assert = require("assert");

/****** Definitions ******/

const NATIVE_FUNCTION_CALL_PREAMBLE = "zkeUHTrVJH_function_call_vgSfXNndgE:";

const ARGS_MAX_CHARS_LIMIT = 2000;

const VALUE_MAX_CHARS_LIMIT = 2000;

/****** Functions ******/

/* Please note that the same client (CDP session) should be used in both
 * functions. Otherwise there won't be any recorded information. */

async function StartProfiler(client, interval = 500) {
    assert(typeof(client) == "object" && typeof(client.send) == "function");
    assert(typeof(interval) == "number" && interval > 0);

    /*
     * Start profiler with sampling interval set to 500 microseconds (default)
     * Sampling Frequency: 2 kHz
     *
     * TODO: Error checking
     */
    await client.send("Profiler.enable");
    await client.send("Profiler.setSamplingInterval", { "interval" : interval });
    await client.send("Profiler.start");
}

async function GetFunctions(client) {
    assert(typeof(client) == "object" && typeof(client.send) == "function");

    let response = await client.send("Profiler.stop");

    assert(typeof(response) == "object");
    assert(typeof(response.profile) == "object");
    assert(typeof(response.profile.nodes) == "object");

    return response.profile.nodes;
}

/* The code of this function will be executed in the browser page */
function InternalNativeFunctionsDetector(preamble, argsLimit, returnValueLimit) {

    function GetCallerFile() {
        let originalFunc = Error.prepareStackTrace;

        let callerfile;
        try {
            const err = new Error();
            let currentfile;

            Error.prepareStackTrace = function (error, stack) {
                return stack;
            };

            currentfile = err.stack.shift().getFileName();
            while (err.stack.length) {
                callerfile = err.stack.shift().getFileName();

                if (currentfile !== callerfile) break; // If you change file stop going down the stack
            }
        } catch (e) {}

        Error.prepareStackTrace = originalFunc;

        return callerfile;
    }

    function RemoveCircleFromObject(parent) {
        let processedTopObject = false;

        return function (key, value) {
            if (processedTopObject &&
                typeof(parent) === "object" &&
                typeof(value) === "object" &&
                parent === value) return "[Circular Reference]";

            processedTopObject = true;

            return value;
        }
    }

    function OverrideFunction(object, property, label) {
        object[property] = (function (orig) {
            return function () {
                let args = arguments;
                const callerFile = GetCallerFile();
                const location = window.location.href;

                /* Call original function */
                let value = orig.apply(this, args);

                let valueToStore = String(value);
                if (value instanceof Array) {
                    valueToStore = JSON.stringify(value, RemoveCircleFromObject(value), 2);
                }
                else if (typeof value === "object") {
                    valueToStore = JSON.stringify(value, RemoveCircleFromObject(value), 2);
                }

                const newLocation = window.location.href;

                RegisterCall({
                    object: label,
                    name: property,
                    args: Array.from(args)
                        .map((arg) => String(arg))
                        .join(", ")
                        .substring(0, argsLimit),
                    returnValue: valueToStore.substring(0, returnValueLimit),
                    location,
                    callerFile,
                    newLocation
                });

                return value;
            };
        })(object[property]);
    }

    function RegisterCall(data) {
        console.log(preamble + JSON.stringify(data));
    }

    /******* Internal Main *******/

    [ "pushState", "replaceState", "back", "forward", "go" ].forEach((func) =>
        OverrideFunction(history, func, "history")
    );

    [ "setItem", "getItem", "removeItem" ].forEach((func) => {
        OverrideFunction(localStorage, func, "localStorage");
        OverrideFunction(sessionStorage, func, "sessionStorage");
    });

    ["random"].forEach((func) =>
        OverrideFunction(Math, func, "Math")
    );

    //OverrideFunction(XMLHttpRequest.prototype, "send", "XMLHttpRequest");
}

const NativeFunctionsDetector = function(page) {

    const state = {
      "functions": [],
      "page": page,
      "isEnabled": true
    };

    function RegisterFunction(data) {
        assert(typeof(data) == "object");
        if (!state.isEnabled) return;
        state.functions.push(data);
    }

    function ProcessMessage(message) {
        if (message.text().startsWith(NATIVE_FUNCTION_CALL_PREAMBLE)) {
            const text = message.text().substring(NATIVE_FUNCTION_CALL_PREAMBLE.length);
            RegisterFunction(JSON.parse(text));
        }
    }

    async function Init() {
        page.on("console", ProcessMessage);
        await page.evaluateOnNewDocument(
            InternalNativeFunctionsDetector,
            NATIVE_FUNCTION_CALL_PREAMBLE,
            ARGS_MAX_CHARS_LIMIT,
            VALUE_MAX_CHARS_LIMIT
        );
    }

    function GetNativeFunctions() {
      return state.functions;
    }

    return {
      Init,
      GetNativeFunctions
    };

  };

/****** Exports ******/

module.exports = {
    StartProfiler,
    GetFunctions,
    NativeFunctionsDetector
};