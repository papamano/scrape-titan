"use strict";

/****** Dependencies ******/

const assert = require("assert");
const exit   = require("process").exit;
const Files  = require(__dirname + "/helpers/files.js");
const Logger = require(__dirname + "/helpers/logger.js")(module);
const config = require(__dirname + "/configs/application.json");

const { Wait } = require(__dirname + "/helpers/time.js");
const { RunService } = require(__dirname + "/helpers/workers.js");

/****** Functions ******/

function SaveResults(results) {
    assert(typeof(results) == "object");

    Logger.info("Writing results to file");
    Files.StoreJsonSync(results, config.output);
}

async function ExitApp(value) {
    assert(typeof(value) == "number");

    /* Necessary to ensure that messages from all threads are printed */
    await Wait(config.exitWait);
    Logger.info("Exiting application");
    exit(value);
}

function CreateWorkerInitData(targets, id) {
    assert(typeof(targets) == "object");
    assert(typeof(id) == "number" && id >= 0);

    return {
        "targets": targets,
        "threadId": id,
        "resultsDir": config.resultsDir,
        "logsDir": config.logsDir
    }
}

function CreateDirectories() {
    Files.EnsureDirectoryExists(config.resultsDir);
    Files.EnsureDirectoryExists(config.logsDir);

    for(var i = 0; i < config.threads; ++i)
        Files.EnsureDirectoryExists(config.resultsDir + "/Thread_" + i);
}


/****** Main ******/

(async function main() {
    try {
        let results = [];
        let threads = [];
        let websites = Files.ReadJsonSync(__dirname + "/" + config.list);

        /* Create a logging object for the main thread */
        Logger.info("Application started");

        /* Make sure that there are enough websites for each thread */
        if (config.threads > websites.length) {
            const errorMsg = "Cannot start application. More working threads (" + config.threads + ") than target websites (" + websites.length + ")";
            Logger.error(errorMsg);
            await ExitApp(config.errorExitValue);
        }

        /* Create directories for collected data */
        CreateDirectories();
        Logger.debug("Created directories");

        for(var i = 0; i < config.threads; ++i) {
            /* Split the list of websites */
            const targets = websites.filter( (value, index) => index % config.threads === i );
            Logger.debug("Thread #" + (i + 1) + " will process " + targets.length + " websites");

            /* Launch worker threads */
            threads.push(RunService(config.workersCode, CreateWorkerInitData(targets, (i + 1))));
            Logger.info("Thread #" + (i + 1) + " was deployed");
        }

        /* Wait for all worker threads to finish */
        Logger.info("Waiting for threads to finish");
        await Promise.all(threads)
        .then(res => res.forEach(entry => results = results.concat(entry.websiteData)))
        .then(_ => Logger.debug("All threads finished successfully"))
        .catch(error => {
            Logger.error("Something went wrong: " + String(error));
        });

        /* Save information about the scraping process */
        Logger.debug("Saving results");
        SaveResults(results);

        Logger.info("Application finished. Exiting");
        await ExitApp(config.exitValue);

    } catch(e) {
        Logger.error("Fatal error: " + String(e));
        ExitApp(config.errorExitValue);
    }

    /* No man's land */
    assert(false);

})();