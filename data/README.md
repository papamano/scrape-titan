# About

This directory contains data necessary for the application to run.

---
### filteredMimeTypes.json

This is a list of MIME types that will be ignored when processing HTTP(S) responses. The response object is processed properly in all cases and information regarding the response object is stored in the respective file. However, the response body (**actual content**) that matches the types in this file will not be stored. This file can be used to exclude binary content from being stored in a JSON file.

---
### filteredWebsites.json

This is a list of websites that should be ignored during the crawling/scraping process. The list should be in JSON format. Log files will still store the names of these websites and that they were skipped but the actual website will neither be visited nor its URL resolved.

---
### websites.json

A list of websites to visit. The list should be in JSON format. These are the websites that will be scraped. This list can contain websites that will be filtered out.
