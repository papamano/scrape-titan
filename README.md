# Scrape Titan

## About

A variation of this tool was used in the following project. If you use this tool or its source code in any way, please cite our work.

Emmanouil Papadogiannakis, Panagiotis Papadopoulos, Nicolas Kourtellis, and Evangelos P Markatos, "[**Leveraging Google's Publisher-Specific IDs to Detect Website Administration**](https://arxiv.org/abs/2202.05074)", In Proceedings of the Web Conference 2022, pages 2522–2531, April 2022.

```
@inproceedings{10.1145/3485447.3512124,
  title = {Leveraging Google’s Publisher-Specific IDs to Detect Website Administration},
  author = {Papadogiannakis, Emmanouil and Papadopoulos, Panagiotis and P. Markatos, Evangelos and Kourtellis, Nicolas},
  year = {2022},
  isbn = {9781450390965},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  url = {https://doi.org/10.1145/3485447.3512124},
  doi = {10.1145/3485447.3512124},
  booktitle = {Proceedings of the ACM Web Conference 2022},
  pages = {2522–2531},
  numpages = {10},
  location = {Virtual Event, Lyon, France},
  series = {WWW '22}
}
```

The paper can be found:
* DOI: [10.1145/3485447.3512124](https://doi.org/10.1145/3485447.3512124)
* Open-access version in [arXiv](https://arxiv.org/abs/2202.05074)

## Introduction

Digital advertising is the most popular way for content monetization on the Internet. Publishers spawn new websites, and older ones change hands with the sole purpose of monetizing user traffic. In this ever-evolving ecosystem, it is challenging to effectively tell: Which entities monetize what websites? What categories of websites does an average entity typically monetize on and how diverse are these websites? How has this website administration ecosystem changed across time?

In this paper, we propose a novel, **graph-based** methodology to detect administration of sites on the Web by exploiting the ad-related **publisher-specific IDs**. We apply our methodology across the top 1M websites and study the characteristics of the created graphs of website administration. Our findings show that approximately 90% of the websites are associated with a single publisher and that small publishers tend to manage less popular websites. We perform a historical analysis of up to 8M websites, and we find a new constantly arising number of (intermediary) publishers that control and monetize traffic from hundreds of websites, seeking a share of the ad-market pie. We see that over the years, websites tend to move from big to smaller administrators.

## Description

This repository contains a tool that can visit websites (emulating a real user) and collect valuable data. The implementation makes use of the [Puppeteer](https://pptr.dev/) library and the [Chromium](https://www.chromium.org/Home/) browser and is able to collect:
* Application-level network traffic (HTTP(S) requests and responses)
* Cookies (both first-party and third-party cookies)
* HTML of website and iFrames
* Executed JavaScript functions
* Screenshots

> More information regarding the collected data can be found in [**scrapers/README.md**](scrapers/README.md).

This tool is configurable and the user is able to select the appropriate parameters based on their needs. Additionally, it can be configured to used multiple threads, each controlling a different and independent browser instance. Finally, the tool supports the use of Chrome/Chromium extensions.

> For more information please read the information in [**configs/README.md**](configs/README.md) and [**data/README.md**](data/README.md).

## Notes

* Additional information and code regarding website administration graphs and their analysis can be found [**here**](https://gitlab.com/papamano/website-administration-graphs).

* This tool is an updated and improved version of the crawling subsystem of [**ConsentGuard**](https://gitlab.com/papamano/consent-guard/).

* This project is publicly available in order to contribute to the scientific community and to support further research.

## Prerequisites

Required Software:

* [curl](https://curl.se/) (tested with version *v7.68.0*)
* [Node.js](https://nodejs.org/en/) (tested with version *v12.22.7*)
* [npm](https://www.npmjs.com/) (tested with version *6.14.15*)

Used Libraries:
* [puppeteer](https://pptr.dev/)
* [winston](https://github.com/winstonjs/winston)
* [adblock-rust](https://github.com/brave/adblock-rust)

Required Hardware:
* N/A

Required Files:
* *See Project Structure*

## Installation

To download the project:
```bash
git clone https://gitlab.com/papamano/scrape-titan
```

To install the required modules:
```bash
cd scrape-titan
npm install
```

To crawl websites and collect data:
```bash
node app.js
```

## Project Structure

```
.
├── configs/
├── data/
├── driver/
├── helpers/
├── scrapers/
├── .gitignore
├── ACKNOWLEDGEMENTS.md
├── app.js
├── EULA.pdf
├── LICENSE
├── package-lock.json
├── package.json
└── README.md
```

* **configs**: Configuration files that affect the behavior of the application.
* **data**: Data files neccessary for the provided functionality
* **driver**: The driving module of the application.
* **helpers**: Source code of helper functions
* **scrapers**: Contains the source code for the scraping operations. Each module in this direcrory is responsible for collecting data from the visited websites.

## Lines of Code

```bash
cloc --exclude-dir=node_modules,logs,Traces --exclude-lang=Markdown,JSON .
```

| Version     | Date        | Files       | Code        | Comments    | Blank Lines |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| v1.0        | 16/02/2022  | 24          | 1144        | 229         | 437         |
| v2.0        | 18/02/2022  | 28          | 1455        | 250         | 548         |

## Roadmap

See the open issues for a list of proposed features and known issues.

## Contributors

* [Papadogiannakis Manos](https://gitlab.com/papamano/)

## Support

Please contact one of the project's contributors.

## License

This project is released under under the Mozilla Public License Version 2.0

Make sure you read the provided End-User License Agreement (EULA).

By installing, copying or otherwise using this Software, you agree to be bound
by the terms of the provided End EULA.