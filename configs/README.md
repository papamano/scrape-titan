# About

This directory contains the configuration files for this application. All config files are in **JSON** format. Various components have their own config file.

## application.json

This file contains the configuration of the main application:

* **threads**: Number of threads to launch. Each thread instruments a different browser instance.
* **workersCode**: The JS file the contains the actual code that each worker thread will execute.
* **list**: List of websites to visit. This list will be partitioned across the different threads and each thread will process a part of this list in parallel.
* **output**: The name of the output file. This file contains very trivial information about the processed websites (e.g. status, time, etc).
* **resultsDir**: The name of the directory that will store the collected data. If the directory does not exist, the application creates it.
* **logsDir**: The name of the directory that will store the log files.  If the directory does not exist, the application creates it.
* **exitWait**: Time period to wait before closing the application AFTER all websites have been processed or a fatal error occurs. Time is measured in milliseconds.
* **exitValue**: The value that the application will return.
* **errorExitValue**: The value that the application will return in case of a fatal error.

## extensions.json

This file contains the paths of extensions that should be loaded in the browser. Each extension should be in a separate path.

## logs.json

This files contains the configuration of the logging system:

* **infoLogPath**: The path of the file that holds messages with `INFO` level.
* **verboseLogPath**: The path of the file that holds messages with `VERBOSE` level.
* **errorsLogPath**: The path of the file that holds messages with `ERROR` level.
* **exceptionsLogPath**: The path of the file that holds exceptions.
* **silent**: Simple flag that indicates whether logs should be suppressed.

## puppeteer.json

This files contains the configuration of the components related to puppeteer:

* **headless**: Flag that indicates if the browser instances will be headless or note. Note that extensions in Chrome / Chromium only work in non-headless mode.
* **processedWebsitesInterval**: Print a message about the progress of the crawling process every X websites. This value is a per-thread configuration parameter.
* **loadPageTimeout**: Time (in seconds) to wait for the main page to load.
* **waitPeriod**: Time (in seconds) to wait after the page has loaded and before exiting.
* **waitUntil**: Flag that indicates when to consider that the navigation succeeded. Valid values: `load`, `domcontentloaded`, `networkidle0` and `networkidle2`.
* **ignoreHTTPSErrors**: Boolean flag to indicate whether to ignore HTTPS errors during navigation.
* **windowWidth**: Window width.
* **windowHeight**: Window height
* **windowPosX**: Window X position
* **windowPosY**: Window Y position
* **devTools**: Flag that indicates whether the devtools tab should be open in the browser.
* **getProtocolTimeout**: Time (in seconds) to wait for the URL protocol to be determined.
* **screenshotTimeout**: Time (in seconds) to wait for the screenshot to be captured.
* **hardTimeout**: Max time (in seconds) to spend in a website.
* **curlResolve**: Boolean flag to indicate whether the target URL should be determined using cURL.
* **acceptOnlyTextAdsTxt**: Boolean flag to indicate whether `ads.txt` files with a content type different from `text/plain` should be accepted.
* **userAgent**: The user-agent string to use when in headless mode.
* **scrollTimeout**: Time (in seconds) to scroll down in the visited website.

## traces.json

This file contains the names of the files that store the collected traces for each website.

* **requestsFile**: Path of file that stores HTTP(S) requests.
* **responsesFile**: Path of file that stores HTTP(S) responses.
* **cookiesFile**: Path of file that stores first-party cookies.
* **thirdPartyCookiesFile**: Path of file that stores third-party cookies.
* **htmlFile**: Path of file that stores captured HTML code.
* **websiteFile**: Path of file that stores information about the target and the landing URLs.
* **functionsFile**: Path of file that stores information about executed JS code (i.e. Profiler output).
* **invalidAdsTxtFile**: Name to use if the `ads.txt` file was not considered valid.
* **firstScreenshotFile**: Name of the first screenshot, taken when the website has just loaded
* **secondScreenshotFile**: Name of the second screenshot, taken just before leaving the website.
