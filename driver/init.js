"use strict";

/****** Dependencies ******/

const assert = require("assert");

const Extensions = require(__dirname + "/../configs/extensions.json");

/****** Functions ******/

function CreateBrowserOptions(config) {
    assert(typeof(config) == "object");
    assert(typeof(config.headless) == "boolean");
    assert(typeof(config.windowWidth) == "number" && config.windowWidth > 0);
    assert(typeof(config.windowHeight) == "number" && config.windowHeight > 0);
    assert(typeof(config.windowPosX) == "number" && config.windowPosX >= 0);
    assert(typeof(config.windowPosY) == "number" && config.windowPosY >= 0);
    assert(typeof(config.devTools) == "boolean");

    /*
     * Chromium command line switches:
     *
     * --disable-web-security: Don't enforce the same-origin policy.
     *                         (Used by people testing their sites.)
     *
     * --disable-features: Comma-separated list of feature names to disable.
     *                     See also kEnableFeatures.
     *
     * --single-process: Runs the renderer and plugins in the same process as
     *                   the browser
     *
     * Site Isolation is a security feature in Chrome that offers additional
     * protection against some types of security bugs. It makes it harder for
     * untrustworthy websites to access or steal information from your accounts
     * on other websites.
     *
     * When debugging with --disable-web-security, it may also be necessary to
     * disable Site Isolation
     * (using --disable-features=IsolateOrigins,site-per-process)
     * to access cross-origin frames.
     *
     *
     * The list of all Chromium command line switches can be found here:
     * https://peter.sh/experiments/chromium-command-line-switches/
     */

    let extensionsPaths = Extensions.join(",");
    if (extensionsPaths.endsWith(",")) extensionsPaths = extensionsPaths.slice(0, -1);

    return {
        headless: config.headless,
        defaultViewport: null,
        ignoreHTTPSErrors: config.ignoreHTTPSErrors,
        devtools: config.devTools,
        args: [
            "--single-process",
            "--disable-web-security",
            "--disable-features=IsolateOrigins,site-per-process",
            "--window-size=" + config.windowWidth + "," + config.windowHeight,
            "--window-position=" + config.windowPosX + "," + config.windowPosY,
            "--disable-extensions-except=" + extensionsPaths,
            "--load-extension=" + extensionsPaths
        ]
    };
}

function CreatePageLoadOptions(config) {
    assert(typeof(config) == "object");
    assert(typeof(config.waitUntil) == "string" && config.waitUntil.length);
    assert(typeof(config.loadPageTimeout) == "number" && config.loadPageTimeout >= 0);

    /* Set to 0 to remove timeout */
    return {
        waitUntil: config.waitUntil,
        timeout: config.loadPageTimeout * 1000
    };
}


/****** Exports ******/

module.exports = {
    CreateBrowserOptions,
    CreatePageLoadOptions,
};