"use strict";

/****** Dependencies ******/

const assert = require("assert");

const FilteredWebsites = new Set(require(__dirname + "/../data/filteredWebsites.json"));

/****** Functions ******/

function IsFilteredWebsite(website) {
    assert(typeof(FilteredWebsites) == "object" && typeof(FilteredWebsites.has) == "function");
    assert(typeof(website) == "string");

    return FilteredWebsites.has(website);
}

/****** Exports ******/

module.exports = {
    IsFilteredWebsite
};