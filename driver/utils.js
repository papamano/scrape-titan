"use strict";

/****** Dependencies ******/

const assert = require("assert");

const Process = require(__dirname + "/../helpers/process.js");
const Logger = require(__dirname + "/../helpers/logger.js")(module);

const { SanitizeUrlName } = require(__dirname + "/../helpers/misc.js");

/****** Definitions ******/

const PROGRESS_BAR_SIZE = 20;

/****** Functions ******/

async function EvadeBotDetection(page) {
    assert(typeof(page) == "object");
    assert(typeof(page.evaluateOnNewDocument) == "function");

    await page.evaluateOnNewDocument(() => {
        Object.defineProperty(navigator, 'webdriver', {
            get: () => false, // This works for chromium
        });

        /* Add a "random" extension to confuse bot-detection scripts */
        Object.defineProperty(navigator, "plugins", {
            get: () => [ {
                "name": "ePHDhQo",
                "filename": "DBAgQIky58.fPnz",
                "length:": 1,
                "description": "BgQo0at27dOHjx4cu379.  fPnz5cOn"
            } ]
        })
    });
}

function CreateEmptyWebsiteData(website) {
    assert(typeof(website) == "string" && website.length);

    return {
        "website": website,
        "success": true,
        "processTime": 0
    };
}

function PrintProgress(workerData, i) {
    assert(typeof(workerData) == "object");
    assert(typeof(workerData.targets) === "object" && workerData.targets.length);
    assert(typeof(workerData.threadId) === "number" && workerData.threadId >= 0);
    assert(typeof(i) == "number" && i >= 0);

    const progress = (100 * ( i + 1 ) / workerData.targets.length).toFixed(2);
    const progressBarFill = parseInt(progress / 100 * PROGRESS_BAR_SIZE);

    const fullBar = "=".repeat(progressBarFill);
    const emptyBar = "-".repeat(PROGRESS_BAR_SIZE - progressBarFill)

    let output = "[" + fullBar + emptyBar + "]" + " [" + progress + "%]"
    output += " [Memory: " + Process.GetMemoryUsageMB() + " MB]"
    Logger.info(output);
}

function CreateTracesDir(threadId, index, website, resultsDir) {
    assert(typeof(threadId) === "number" && threadId >= 0);
    assert(typeof(index) === "number" && index >= 0);
    assert(typeof(website) == "string" && website.length);
    assert(typeof(resultsDir) == "string" && resultsDir.length);

    return resultsDir + "/Thread_" + threadId + "/" + index + "." + SanitizeUrlName(website);
}

/****** Exports ******/

module.exports = {
    EvadeBotDetection,
    CreateEmptyWebsiteData,
    PrintProgress,
    CreateTracesDir
};