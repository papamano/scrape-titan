"use strict";

/****** Dependencies ******/

const assert   = require("assert");
const Utils    = require(__dirname + "/utils.js");
const Network  = require(__dirname + "/../helpers/network.js");
const Logger   = require(__dirname + "/../helpers/logger.js")(module);
const config   = require(__dirname + "/../configs/puppeteer.json");

const { workerData, parentPort } = require("worker_threads");
const { IsFilteredWebsite }      = require(__dirname + "/filter.js");
const { ScrapeWebsite }          = require(__dirname + "/scrape.js");

/****** Functions ******/

async function GetTargetUrl(website) {
    assert(typeof(website) == "string" && website.length);

    let targetURL = null;
    let curlFailed = false;

    if (config.curlResolve) {
        Logger.debug("Resolving URL for '" + website + "'");
        targetURL = await Network.ResolveUrl(website, config.getProtocolTimeout).catch(_ => { curlFailed = true; return null; });
        Logger.debug("URL '" + website + "' was resolved to '" + targetURL + "'");
    }

    if (!config.curlResolve || curlFailed) {
        Logger.debug("Getting protocol for '" + website + "'");
        const protocol = await Network.GetProtocol(website, config.getProtocolTimeout * 1000);
        targetURL = protocol + website;
        Logger.debug("URL '" + website + " was resolved to '" + targetURL + "'");
    }

    assert(typeof(targetURL) == "string" && targetURL.length);
    return targetURL;
}

async function ProcessWebsite(website, directory) {
    assert(typeof(website) == "string" && website.length);
    assert(typeof(directory) == "string" && directory.length);

    const targetURL = await GetTargetUrl(website);
    assert(typeof(targetURL) == "string" && targetURL.length);

    const data = await ScrapeWebsite(website, targetURL, directory);
    return data;
}

/****** Main ******/

(async function main() {
    assert(typeof(workerData) == "object");
    assert(typeof(workerData.targets) === "object" && workerData.targets.length);
    assert(typeof(workerData.threadId) === "number" && workerData.threadId >= 0);
    assert(typeof(workerData.resultsDir) === "string" && workerData.resultsDir.length);
    assert(typeof(workerData.logsDir) === "string" && workerData.logsDir.length);

    let results = [];

    Logger.info("Thread #" + workerData.threadId + " started. Will process " + workerData.targets.length + " websites");

    for(var i = 0; i < workerData.targets.length; ++i) {
        /* Print the progress of the current worker thread */
        Utils.PrintProgress(workerData, i);

        /* Get next website in the list */
        const website = workerData.targets[i];
        Logger.debug("Processing website '" + website + "' with index " + i);

        /* Skip filtered websites */
        if (IsFilteredWebsite(website)) {
            results.push({ website, filtered: true });
            Logger.info("Website '" + website + "' was filtered out");
            continue;
        }

        /* Create one directory for each website. In case of failure, this
         * directory will be removed/deleted. */
        const targetDir = Utils.CreateTracesDir(workerData.threadId, i, website, workerData.resultsDir);

        /* Visit website and collect data */
        let result = await ProcessWebsite(website, targetDir);
        Logger.debug("Website '" + website + "' was processed");

        /* Store scraping result */
        results.push(result);
    }

    Logger.debug("Finished. All websites were processed");
    Logger.info("Thread " + workerData.threadId + " finished processing " + workerData.targets.length + " websites")

    parentPort.postMessage({
        "websiteData": results
    });

    Logger.info("Thread " + workerData.threadId + " sent data to master thread. Exiting");
    return;

})();