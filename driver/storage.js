"use strict";

/****** Dependencies ******/

const assert = require("assert");
const Files = require(__dirname + "/../helpers/files.js");
const Logger = require(__dirname + "/../helpers/logger.js")(module);
const tracesConfig = require(__dirname + "/../configs/traces.json");

/****** Functions ******/

function ClearArray(data) {
    assert(typeof(data) == "object" && typeof(data.length) == "number");
    data.splice(0, data.length);
}

function ClearCachedTraces(traces) {
    assert(typeof(traces) == "object");

    for (const key in traces) {
        if (typeof(traces[key]) == "object" &&
            typeof(traces[key].length) == "number")
            ClearArray(traces[key]);

        traces[key] = null;
        delete traces[key];
    }
}

async function StoreTraces(traces, targetDir) {
    assert(typeof(targetDir) == "string" && targetDir.length);
    assert(typeof(traces) == "object");
    assert(typeof(traces.websites) == "object");
    assert(typeof(traces.requests) == "object");
    assert(typeof(traces.responses) == "object");
    assert(typeof(traces.cookies) == "object");
    assert(typeof(traces.thirdPartyCookies) == "object");
    assert(typeof(traces.html) == "string" && traces.html.length);
    assert(typeof(traces.functions) == "object");
    assert(typeof(traces.nativeFunctions) == "object");
    assert(typeof(traces.urls) == "object");
    assert(typeof(traces.ads) == "object");

    Files.EnsureDirectoryExists(targetDir);

    let promises = [];

    Logger.debug("Storing data in '" + targetDir + "'");
    promises.push(Files.StoreJson(traces.requests,          targetDir + tracesConfig.requestsFile));
    promises.push(Files.StoreJson(traces.responses,         targetDir + tracesConfig.responsesFile));
    promises.push(Files.StoreJson(traces.cookies,           targetDir + tracesConfig.cookiesFile));
    promises.push(Files.StoreJson(traces.thirdPartyCookies, targetDir + tracesConfig.thirdPartyCookiesFile));
    promises.push(Files.StoreJson(traces.websites,          targetDir + tracesConfig.websiteFile));
    promises.push(Files.StoreJson(traces.functions,         targetDir + tracesConfig.functionsFile));
    promises.push(Files.StoreJson(traces.nativeFunctions,   targetDir + tracesConfig.nativeFunctionsFile));
    promises.push(Files.StoreJson(traces.urls,              targetDir + tracesConfig.urlsFile));
    promises.push(Files.StoreJson(traces.ads,               targetDir + tracesConfig.adsFile));
    promises.push(Files.StoreString(traces.html,            targetDir + tracesConfig.htmlFile));

    /* Wait for all the data to be stored */
    await Promise.all(promises).catch(error => { throw String(error) });
    Logger.info("Data stored in '" + targetDir + "'");
}

/****** Exports ******/

module.exports = {
    ClearCachedTraces,
    StoreTraces
};