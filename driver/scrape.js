"use strict";

/****** Dependencies ******/

const assert    = require("assert");
const puppeteer = require("puppeteer");
const Scrapers  = require(__dirname + "/../scrapers");
const Utils     = require(__dirname + "/utils.js");
const Init      = require(__dirname + "/init.js");
const Time      = require(__dirname + "/../helpers/time.js");
const Files     = require(__dirname + "/../helpers/files.js");
const Logger    = require(__dirname + "/../helpers/logger.js")(module);

const config = require(__dirname + "/../configs/puppeteer.json");
const tracesConfig = require(__dirname + "/../configs/traces.json");

const { ScrollForMs } = require(__dirname + "/../helpers/scroller.js");
const { StoreTraces, ClearCachedTraces } = require(__dirname + "/storage.js");

/****** Definitions ******/

const PAGE_OPTIONS = Init.CreatePageLoadOptions(config);
const BROWSER_OPTIONS = Init.CreateBrowserOptions(config);

/****** Functions ******/

async function Scrape(browser, targetURL, listName, directory) {
    assert(typeof(browser) == "object");
    assert(typeof(browser.newPage) == "function");
    assert(typeof(targetURL) == "string" && targetURL.length);
    assert(typeof(listName) == "string" && listName.length);
    assert(typeof(directory) == "string" && directory.length);

    const pages = await browser.pages();
    assert(pages.length);
    const page = pages[0];
    Logger.debug("Retrieved new page instance", { metadata: targetURL });

    const client = await page.target().createCDPSession();
    assert(typeof(client) == "object");

    /* Evade bot detection */
    await Utils.EvadeBotDetection(page);
    if (config.headless) await page.setUserAgent(config.userAgent);
    Logger.debug("Enabled bot-detection evasion", { metadata: targetURL });

    /* Monitor application-level network traffic */
    const networkMonitor = new Scrapers.NetworkMonitor();
    page.on("request", networkMonitor.RegisterRequest);
    page.on("response", networkMonitor.RegisterResponse);

    /* Start the profiler before visiting the website */
    await Scrapers.StartProfiler(client);

    /* Prepare the infrastructure that monitors native functions before visiting
     * the website */
    const functionsDetector = new Scrapers.NativeFunctionsDetector(page);
    await functionsDetector.Init();

    /* Visit website */
    await page.goto(targetURL, PAGE_OPTIONS);
    Logger.info("Navigated to '" + targetURL + "'");

    /*
     * Scraping
     *
     * 0. Monitor and store network traffic (HTTP(S) requests and reponses)
     * 1. Screenshot of landing page
     * 2. Scroll for X amount of tie
     * 3. Get HTML
     * 4. Extract stored cookies (both first-party and third-party cookies)
     * 5. Extract executed JavaScript functions
     * 6. Extract page URLs
     * 7. Take new screenshot
     * 8. Detect and visit ads
     * 9. Store URL info (list URL, target URL, landing URL)
     */
    let status = await Scrapers.TakeScreenshot(page, directory, config.screenshotTimeout, tracesConfig.firstScreenshotFile).catch(_ => false);
    Logger.debug((status ? "Captured screenshot" : "Failed to take screenshot"), { metadata: targetURL });

    const landingWebsite = page.url();

    await ScrollForMs(page, config.scrollTimeout * 1000);
    Logger.debug("Scrolled down for " + config.scrollTimeout + " sec", { metadata: targetURL });

    await Time.Wait(config.waitPeriod * 1000);
    Logger.debug("Waited for " + config.waitPeriod + " sec for content to load", { metadata: targetURL });

    const html = await Scrapers.GetPageContent(page);
    Logger.debug("Captured HTML content ", { metadata: targetURL });

    const firstPartyCookies = await Scrapers.GetFirstPartyCookies(page);
    Logger.debug("Captured 1st-party cookies", { metadata: targetURL });

    const thirdPartyCookies = await Scrapers.GetThirdPartyCookies(page);
    Logger.debug("Captured 3rd-party cookies", { metadata: targetURL });

    const usedFunctions = await Scrapers.GetFunctions(client);
    Logger.debug("Retrieved JavaScript function calls", { metadata: targetURL });

    const links = await Scrapers.GetPageLinks(page);
    Logger.debug("Extracted " + links.length + " URLs from page", { metadata: targetURL });

    status = await Scrapers.TakeScreenshot(page, directory, config.screenshotTimeout, tracesConfig.secondScreenshotFile).catch(_ => false);
    Logger.debug((status ? "Captured screenshot" : "Failed to take screenshot"), { metadata: targetURL });

    networkMonitor.StopRegistering();
    page.off("request", networkMonitor.RegisterRequest);
    page.off("response", networkMonitor.RegisterResponse);
    Logger.debug("Stopped monitoring network traffic " + Time.GetDateTime(), { metadata: targetURL });

    if (config.doRandomWalk) {
        await Scrapers.DoRandomWalk(page);
        Logger.debug("Performed random walk across website and external URLs", { metadata: targetURL });
    }

    const nativeFunctions = await functionsDetector.GetNativeFunctions();
    Logger.debug("Retrieved native function calls for", { metadata: targetURL });

    const adData = { };

    if (config.detectAds) {
        adData.adUrls = await Scrapers.GetPageAds(links, landingWebsite, networkMonitor.GetResponses());
        Logger.debug("Extracted " + adData.adUrls.length + " ad URLs", { metadata: targetURL });

        adData.ads = await Scrapers.VisitAds(page, adData.adUrls, config.adVisitTimeout * 1000);
        Logger.debug("Visited " + adData.ads.length + " ad landing pages", { metadata: targetURL });
    }

    const urls = {
        "listWebsite": listName,
        "targetWebsite": targetURL,
        "landingWebsite": landingWebsite
    };

    /* Close browser instance */
    await client.detach()
    await browser.close();
    Logger.debug("Scraping over. Browser instance closed", { metadata: targetURL });

    return {
        "requests": networkMonitor.GetRequests(),
        "responses": networkMonitor.GetResponses(),
        "html": html,
        "cookies": firstPartyCookies,
        "thirdPartyCookies": thirdPartyCookies,
        "functions": usedFunctions,
        "nativeFunctions": nativeFunctions,
        "ads": adData,
        "urls": links,
        "websites": urls
    };
}

async function ScrapeWithTimeout(browser, targetURL, listName, timeout, directory) {
    assert(typeof(browser) == "object");
    assert(typeof(browser.newPage) == "function");
    assert(typeof(targetURL) == "string" && targetURL.length);
    assert(typeof(listName) == "string" && listName.length);
    assert(typeof(timeout) == "number" && timeout > 0);

    return new Promise((resolve, reject) => {
        Scrape(browser, targetURL, listName, directory).then(resolve).catch(reject);

        /* Forcefully kill browser instance if it takes too long. There are
         * known issues where sometimes puppeteer hangs:
         *
         * https://github.com/puppeteer/puppeteer/issues/4011
         */
        setTimeout(_ => {
            browser.process().kill("SIGKILL");
            reject("Hard timeout of " + timeout + " ms exceeded");
        }, timeout);

    });
}

async function CollectTraces(website, listName, directory) {
    assert(typeof(website) == "string" && website.length);
    assert(typeof(listName) == "string" && listName.length);
    assert(typeof(directory) == "string" && directory.length);

    let browser = null;

    let data = Utils.CreateEmptyWebsiteData(website);

    let startTime = Time.StopwatchClick();

    try {
        /* Launch one browser instance for each visited website */
        browser = await puppeteer.launch(BROWSER_OPTIONS);
        assert(typeof(browser) == "object");
        Logger.debug("Browser instance created", { metadata: website });

        /* Scraping */
        Logger.debug("Collecting traces", { metadata: website });
        const traces = await ScrapeWithTimeout(browser, website, listName, config.hardTimeout * 1000, directory);
        assert(typeof(traces) == "object");
        Logger.debug("Collected traces", { metadata: website });

        /* Store collected data */
        await StoreTraces(traces, directory);
        Logger.debug("Stored traces", { metadata: website });

        ClearCachedTraces(traces);

    } catch (err) {
        let errorMsg = "Error at " + website + ": " + String(err).trim();
        Logger.error(errorMsg, { metadata: website });

        data.success = false;
        data.error = String(err);

        /* If the scraping process failed, make sure that the browser instance
         * is no longer running in background */
        if (browser) browser.close();
        Logger.debug("Closed hanging browser instance", { metadata: website });

        /* For websites that the scraping process failed we don't want to store
         * any data */
        Files.RemoveDirectory(directory);
        Logger.debug("Scraping failed. Removed directory '" + directory + "'", { metadata: website });
    }

    data.processTime = Time.GetElapsedTime(startTime, Time.StopwatchClick());
    Logger.info("Fully scraped '" + website + "' in " + data.processTime + " secs");

    return data;
}

async function GetAdsTxt(website, directory) {
    assert(typeof(website) == "string" && website.length);
    assert(typeof(directory) == "string" && directory.length);

    Logger.debug("Trying to download ads.txt file", { metadata: website });

    /* Download the ads.txt file (if available) */
    const retrievedAds = await Scrapers.GetAdsTxt(website, directory);
    assert(typeof(retrievedAds) == "object" && typeof(retrievedAds.success) == "boolean");
    Logger.debug((retrievedAds.success ? "Fetched" : "Failed to fetch") + " ads.txt file", { metadata: website });

    /* Discard invalid ads.txt files that do not follow the standard */
    if (Scrapers.IsInvalidAdsTxt(retrievedAds, config.acceptOnlyTextAdsTxt)) {
        retrievedAds.success = false;
        Files.RenameFile(directory + "/ads.txt", directory + tracesConfig.invalidAdsTxtFile);
        Logger.warn("Ignored invalid ads.txt file with content type '" + retrievedAds.contentType + "'", { metadata: website });
    }

    return retrievedAds;
}

async function ScrapeWebsite(website, targetURL, directory) {
    assert(typeof(website) == "string" && website.length);
    assert(typeof(targetURL) == "string" && targetURL.length);
    assert(typeof(directory) == "string" && directory.length);

    Files.EnsureDirectoryExists(directory);

    /* Download the ads.txt file separately before visiting the website. There
     * is no need for a browser instance here */
    const retrievedAds = await GetAdsTxt(targetURL, directory);
    assert(typeof(retrievedAds) == "object" && typeof(retrievedAds.success) == "boolean");

    /* Launch a browser instance and visit the website, emulating a real user */
    const data = await CollectTraces(targetURL, website, directory);

    data.retrievedAds = retrievedAds.success;
    data.website = website;
    data.landingURL = targetURL;
    return data;
}

/****** Exports ******/

module.exports = {
    ScrapeWebsite
};