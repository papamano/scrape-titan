# The program used as the shell is taken from the variable `SHELL'.  If this
# variable is not set in your makefile, the program `/bin/sh' is used as the
# shell.
SHELL := /bin/bash

STDOUT_LOG_FILE="logs/crawler_stdout.log"
STDERR_LOG_FILE="logs/crawler_stderr.log"

.PHONY: clean distclean

help:
	@echo "Supported commands: 'run', 'clean', 'distclean'"

logs:
	@mkdir -p logs/

run: logs
	@/usr/bin/time --format=">>> Execution Time: [ %E ]" node app.js > >(tee -a $(STDOUT_LOG_FILE)) 2> >(tee -a $(STDERR_LOG_FILE) >&2)
#time is both a bash keyword and a program in /usr/bin. We want to use the program

clean:
	@echo "Cleaning logs"
	@rm -rf logs/

distclean: clean
	@echo "Cleaning results"
	@rm -rf Traces/